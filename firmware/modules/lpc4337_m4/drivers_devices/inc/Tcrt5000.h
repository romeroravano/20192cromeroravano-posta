/*
 * Tcrt5000.h
 *
 *  Created on: 6 sep. 2019
 *      Author:
 * BENITEZ CERQUETELLA, VIRGINIA - vir_be@hotmail.com
 * CHACÓN, ALEJO - alejochacon96@hotmail.com
 * ROMERO RAVANO, MARIA VICTORIA - victoriaremo@gmail.com
 * RATTI, TOMÁS - tomasratti@gmail.com
 */

#ifndef TCRT5000_H_
#define TCRT5000_H_
#include "gpio.h"

/**@brief Funcion que inicializa  el sensor de proximidad infrarrojo Tcrt5000
 * setea la direccion del puerto GPIO para el pin de salida del sensor
 * el puerto en uso se guarda en gpio_t MiPin
 *
 * @param[in] dout Direccion del puerto GPIO a utilizar
 *
 * @return (booleano) TRUE si el puerto se inicializo correctamente
 */

bool Tcrt5000Init(gpio_t dout);

/**@brief Funcion para obtener el estado actual del puerto de salida de Tcrt5000
 *
 * @param[in] ninguno
 *
 * @return (booleano) FALSE cuando detecta un objeto
 */

bool Tcrt5000State(void);

/**@brief Funcion para desinicializar el puerto
 *
 * @param[in] ninguno
 *
 * @return
 */


bool Tcrt5000Deinit(gpio_t dout);

#endif /* MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_TCRT5000_H_ */
