
	Recuperatorio, Teoría.
			
	1) a) Una excepción es un evento que "modifica" el flujo normal del programa. Ante una excepción el programa suspende su 
		ejecución (lo que está haciendo), se redirige a otra dirección (Handler de excepción) guardando la dirección de memoria 
		de donde se encontraba, realiza las tareas necesarias y una vez concretado todo vuelve a donde se encontraba y continua
		ejecutandose normalmente.
		El término de interrupción es similar a excepción según la clasificación ARM, la diferencia es que una interrupción es llamada
		por perifericos (UART/USART), temporizadores, etc. y se puede elegir el orden de prioridad, la tarea a realizar, entre otros;
		en cambio una excepción es propio del microcontrolador y lo único que se puede modificar es el orden de prioridad, 
		exceptuando 3 excepciones que no se pueden modificar en absoluto (que se encuentran en el orden -3,-2 y -1).
		
		b) Lo que se aloja en estas direcciones de memoria es donde fueron llamadas las subrutinas del Handler de excepción. Es decir,
		Cuando un programa llama a un subprograma (subrutina), se guarda la dirección de memoria para que una vez finalizada la tarea
		de la subrutina se pueda volver al lugar donde se la llamó para que el programa se siga ejecutando.
			
			
			
			
	2)
	
	Funcionamiento del RIT timer:
	
	El RIT( temporizador de interrupciones repetitivas) es un contador de 32 bits que en vez de utilizar la lógica del Systick (tiempo real) lo hace
	de manera específica.
	
	-El reset activa el contador (32-bit counter) y este empieza a contar a partir de 0x 0000 0000.
	-Cada dato del contador es comparado con un valor ya especificado (COMPVAL) en el comparador.
	-Si los datos son IGUALES se "activa" la interrupción.
	-El bit RITENCLR (timer enamble clear) si es '0' habilita la interrupcion cuando se realiza esta comparacion si son iguales pero no interrumpe al 
	 contador(sigue contando hasta 0x FFFF FFFF) en cambio si este bit es igual a '1' además de habilitar la interrupción, reinicia al contador cuando 
	 se iguala el valor del contador con el dato del comparador ya especificado.
	
	
	a) La función que cumplen las compuertas OR a la salida del comparator es agregar a un valor un '1' proveniente del MASK REGISTER para 
	   eliminar/deshabilitar dicho valor si se requiere. 		