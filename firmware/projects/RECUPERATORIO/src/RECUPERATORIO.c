/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es: ROMERO RAVANO, MARIA VICTORIA
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "analog_io.h"
#include "timer.h"
#include "spi.h"
#include "gpio.h"
#include "string.h"
#include "../inc/RECUPERATORIO.h"
#include "delay.h"
#include "uart.h"
#include "DisplayITS_E0803.h"
/*==================[macros and definitions]=================================*/

uint16_t contador= 100; //porque la frecuencia es de 100 muestras en un segundo
uint16_t valor=0; //en esta variable voy a ir guardando las muestras
/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
void ReadSignal (void)
{

	 AnalogInputRead(CH1,&valor);

}
void AtencionInterrupcion (void)
{

	AnalogStartConvertion();
	contador--;

}

void funcion_nula (void)
{
}


int main(void)
{
	uint16_t valor_filtrado=0;
	uint16_t lpf_beta=0;
	uint16_t aux=0;

	//Inicialización del LCD
	gpio_t pins[7];
	pins[0]=LCD1;
	pins[1]=LCD2;
	pins[2]=LCD3;
	pins[3]=LCD4;
	pins[4]=GPIO1;
	pins[5]=GPIO3;
	pins[6]=GPIO5;

	ITSE0803Init(pins);


	SystemClockInit();

	SwitchesInit(); //Inicialización de las teclas


	//Inicialización de Conversor AD

	analog_input_config signal;
	signal.input=CH1;
	signal.mode=AINPUTS_SINGLE_READ;
	signal.pAnalogInput=&ReadSignal;

	//Inicialización de Interrupciones
	timer_config temp;
	temp.timer=TIMER_B;
	temp.period=10;
	temp.pFunc=AtencionInterrupcion;//Cada 10 ms arranca la conversión. Termina la conversión
									//y arranca la función ReadSignal donde se lee el valor.
	TimerInit(&temp);

	TimerStart(TIMER_B);

	AnalogInputInit(&signal);

	//Inicialización serial
	serial_config puerto;
	puerto.port = SERIAL_PORT_P2_CONNECTOR;
	puerto.baud_rate=9600;
	puerto.pSerial=funcion_nula;

	UartInit(&puerto);

	int8_t teclas= SwitchesRead();


    while(1)
{


    		 switch(teclas)
    		    	{
    		 	 	 	case SWITCH_2:		//Tecla 2

    		 	 	 	lpf_beta=0.25;


    		 	 	 	 break;

    		 	 	 	case SWITCH_3:	//Tecla 3

    		 	 	 	lpf_beta=0.55;


    		    		break;

    		    		case SWITCH_4:	//Tecla 4}

    		    		lpf_beta=0.75;


    		    		break;
    		    	}

    	if (contador>0)
    		    	{
    		    		valor_filtrado= valor_filtrado - lpf_beta*valor_filtrado - lpf_beta*valor;

    		    	 //El envío de la cadena a la PC se repite periódicamente
    		    	   UartSendString(SERIAL_PORT_PC,UartItoa(valor,10));
    		    	   UartSendString(SERIAL_PORT_PC," , \n\r");
    		    	   UartSendString(SERIAL_PORT_PC,UartItoa(valor_filtrado,10));
    		    	 //Muestro solo la parte decimal del lpf_beta actual por LCD
    		    	   if(lpf_beta=0.25){

    		    		   aux=25;
    		    		   ITSE0803DisplayValue(aux);
    		    	   	   	   	   	   	  }

    		    	   if(lpf_beta=0.55){

    		    		   aux=55;
    		    		   ITSE0803DisplayValue(aux);
    		    	     		    	 }

    		    	   if(lpf_beta=0.75){

    		    		   aux=75;
    		    		   ITSE0803DisplayValue(aux);

    		    	     		    	 }

    		    	}
    	else
    			{
    				contador=100; //se repite el ciclo despues de guardar los valores
    			}





} //fin while



	return 0;
}

/*==================[end of file]============================================*/

