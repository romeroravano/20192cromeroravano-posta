/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es: ROMERO RAVANO, MARIA VICTORIA
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/parcial.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "analog_io.h"
#include "timer.h"
#include "spi.h"
#include "gpio.h"
#include "string.h"
#include "delay.h"
#include "DisplayITS_E0803.h"
/*==================[macros and definitions]=================================*/

uint16_t contador= 250; //porque la frecuencia es de 250 muestras en un segundo
uint16_t valor=0; //en esta variable voy a ir guardando las muestras

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
void ReadSignal (void)
{

	 AnalogInputRead(CH1,&valor);

}
void AtencionInterrupcion (void)
{

	AnalogStartConvertion();
	contador--;

}

void funcion_nula (void)
{
}


int main(void)
{
	uint16_t teclas,MAX,MIN,PROM;
	MAX=0;  //presion maxima
	MIN=0;  //presion minima
	PROM=0; //promedio de presiones

	//Inicialización del display
	gpio_t pins[7];
	pins[0]=LCD1;
	pins[1]=LCD2;
	pins[2]=LCD3;
	pins[3]=LCD4;
	pins[4]=GPIO1;
	pins[5]=GPIO3;
	pins[6]=GPIO5;

	ITSE0803Init(pins);


	SystemClockInit();

	LedsInit(); //Inicialización de los leds

	SwitchesInit(); //Inicialización de las teclas


	//Inicialización de Conversor AD
	analog_input_config signal;
	signal.input=CH1;
	signal.mode=AINPUTS_SINGLE_READ;
	signal.pAnalogInput=&ReadSignal;

	//Inicialización de Interrupciones
	timer_config temp;
	temp.timer=TIMER_B;
	temp.period=4;
	temp.pFunc=AtencionInterrupcion;//Cada 4 ms arranca la conversión. Termina la conversión
									//y arranca la función ReadSignal donde se lee el valor.
	TimerInit(&temp);
	TimerStart(TIMER_B);
	AnalogInputInit(&signal);

	//Inicialización serial
	serial_config puerto;
	puerto.port = SERIAL_PORT_P2_CONNECTOR ;
	puerto.baud_rate=9600;
	puerto.pSerial=funcion_nula;
	UartInit(&puerto);

	teclas= SwitchesRead();

	 switch(teclas)
	    	{
	 	 	 	case SWITCH_1:		//Tecla 1= muestra en display la presion minima y la envia a la pc
	 	 	 	LedOn(LED1); //se prende LED AMARILLO
	 	 	 	LedOff(LED2);
	 	 	 	LedOff(LED3);
	 	 	 	ITSE0803DisplayValue(MIN);
	 	 	 	UartSendString(SERIAL_PORT_PC,UartItoa(MIN,10));
	 	 	 	UartSendString(SERIAL_PORT_PC," mmHg \n\r");

	 	 	 	 break;
	 	 	 	case SWITCH_2:	//Tecla 2= muestra en display la presion maxima y la envia a la pc
	 	 	    LedOn(LED2);  //se prende LED ROJO
	 	 	    LedOff(LED1);
	 	 	  	LedOff(LED3);
	 	 	 	ITSE0803DisplayValue(MAX);
	 	 	 	UartSendString(SERIAL_PORT_PC,UartItoa(MAX,10));
	 	 	 	UartSendString(SERIAL_PORT_PC," mmHg \n\r");
	    		break;
	    		case SWITCH_3:	//Tecla 3= muestra en display la presion maxima y la envia a la pc
	    		LedOn(LED3);  //se prende LED VERDE
	    		LedOff(LED2);
	    		LedOff(LED1);
	    		ITSE0803DisplayValue(PROM);
	    		UartSendString(SERIAL_PORT_PC,UartItoa(PROM,10));
	    		UartSendString(SERIAL_PORT_PC," mmHg \n\r");
	    		break;
	    	}

 uint16_t aux1,aux2,aux3,comp1; //variables que utilizo para comparar con los valores que se van leyendo
 aux1=0;
 aux2=5000;
 aux3=0;
 comp1=(200*150)/3.3;  //equivalente a 150 mmHg
 comp2=(200*50)/3.3;	//equivale a 50 mmHg
    while(1)
{
    	if (contador>0)
    		    	{
    		    		if (valor>aux1)	//obtengo el maximo valor
    		    		{
    		    			aux1=valor;
    		    		}
    		    		if(valor<aux2)//obtengo el minimo valor
    		    		{
    		    			aux2=valor;
    		    		}

    		    		aux3=aux3+ valor;//obtengo la sumatoria de los 250 valores

    		    	}
    	else
    			{
    				contador=250; //se repite el ciclo despues de guardar los valores

    		    	//convierto los valores a mmHG
    		    	MAX= (aux1*200)/3.3;
    		    	MIN=(aux2*200)/3.3;
    		    	aux3=aux3/250;//obtengo el promedio de los valores
    		    	PROM=(aux3*200)/3.3;

    		    	if(MAX>comp1) //comparo si MAX es mayor a 150 mmHg
    		    	{
    		    		LedOn(LED_RGB_R);//prendo LED ROJO
    		    		LedOff(LED_RGB_G);
    		    		LedOff(LED_RGB_B);
    		    	}

    		    	}
    		    	if(comp2<MAX<comp1) //comparo si MAX se encuentra entre 50 y 150 mmHg
    		    	{
    		    		LedOn(LED_RGB_B);//PRENDO LED AZUL
    		    		LedOff(LED_RGB_R);
    		    		LedOff(LED_RGB_G);
    		    	}
    		    	if(MAX<comp2)		//comparo si MAX es menor a 50mmHg
    		    	{
    		    	    LedOn(LED_RGB_G);//PRENDO LED VERDE
    		    	    LedOff(LED_RGB_R);
    		    	    LedOff(LED_RGB_B);
    		    	}


    			}

} //fin while



	return 0;
}

/*==================[end of file]============================================*/

