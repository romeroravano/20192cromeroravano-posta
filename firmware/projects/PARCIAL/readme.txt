PARCIAL PRACTICA

Esta adquisición leera cada 4 mS 250 valores obtenidos del sensor de presion en un segundo ciclicamente.

Únicamente se podrá mostrar un valor en display al presionar las teclas de la EDU-CIAA correspondientes (tecla 1=minimo valor, 
tecla 2=maximo valor, tecla 3= promedio de los 250 valores). Así mismo, interpreté que cada valor correspondiente a cada 
tecla se envia a la PC solo si se apreta dicha tecla(ej, presiono tecla 1, me muestra en display el minimo valor, se prende el
led amarillo y se envia dicho valor a la PC)

También interpreté que los LEDs RGB siempre se deben prender (inciso d) como señal de alarma cada 1 segundo, no sólo cuando presiono la tecla 2.
