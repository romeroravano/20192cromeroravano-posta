
1) Indique el  nombre del conversor de la figura y explique su funcionamiento.

EL conversor de la figura es un conversor digital/analogico de aproximaciones sucesivas.
Este conversor recibe las entradas digitales del registro de aproximaciones sucesivas(SAR), luego
la tension de salida del conversor se compara con una tension de entrada y si son iguales se envía la señal analógica.

Defina y describa las diferencias entre tiempo de conversión y tiempo de adquisición.

tiempo de conversión= es el tiempo que demora en convertir la señal.
tiempo de adquisicion= intervalo de tiempo necesario para igualar la tension de salida con la tension de referencia, con 
un margen de error antes especificado.

¿Qué función cumple el bloque S/H (Sample and Hold) ?

la funcion que cumple es guardar cada tension de entrada que recibe(muestra) en el capacitor(sample) cada vez que se cierra
la llave (hold) y cuando esta de abre el capacitor se descarca enviando la tensión que tenía al comparador.


Describa el funcionamiento del SysTick empleando el diagrama de bloques de la figura

El temporizador SysTick es un contador de 24 bits decreciente 