/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "botones_eoc.h"
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "analog_io.h"
#include "timer.h"
#include "bool.h"
#include "spi.h"
#include "ili9341.h"
#include "gpio.h"
#include "string.h"
#include "delay.h"

/*==================[macros and definitions]=================================*/

#define LIMITE_INF 475 //Equivale a 1,62V
#define LIMITE_SUP	540 //Equivale a 1,74V
#define SI 2
#define NO 1
#define NULO 0
bool muestreo = false;
uint16_t respuesta,valor;
bool estado;

uint8_t tecla1;
uint8_t tecla2;



/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/



void ReadSignal (void){
	 AnalogInputRead(CH1,&valor);
	 muestreo=true;
}
void AtencionInterrupcion (void){
	AnalogStartConvertion();
}

uint16_t Respuesta(){
	if(muestreo==true){

	if((valor>=LIMITE_INF)&&(valor<=LIMITE_SUP)){
		estado=true;
		respuesta=NULO;
		LedOn(LED_RGB_B);
		LedOn(LED_RGB_G);
		LedOn(LED_RGB_R);
	}
	else{
		estado=false;
		if(valor>LIMITE_SUP){
			respuesta=SI;
			LedsOffAll();
			LedOn(LED_3);
						}
		else{
			respuesta=NO;}
			LedsOffAll();
			LedOn(LED_2);
			}
		}
	return respuesta;
}



int main(void)
{
	SystemClockInit();
	LedsInit();

	//Inicialización de Conversor AD
	analog_input_config signal;
	signal.input=CH1;
	signal.mode=AINPUTS_SINGLE_READ;
	signal.pAnalogInput=ReadSignal;

	//Inicialización de Interrupciones
	timer_config temp;
	temp.timer=TIMER_B;
	temp.period=10;
	temp.pFunc=AtencionInterrupcion;//Cada cierto tiempo arranca la conversión. Termina la conversión
										//y arranca la función ReadSignal donde se lee el valor.

	TimerInit(&temp);
	TimerStart(TIMER_B);
	AnalogInputInit(&signal);





	//Inicialización y variables de LCD

		uint16_t x1,y1,x2,y2,x3,y3,x4,y4,color,fondo;
		char oracion1[50],oracion2[50],oracion3[50],oracion4[50];
		char *ptr1,*ptr2,*ptr3,*ptr4;
		Font_t fuente1,fuente2;
		Font_t* ptr5;
		bool bandera1,bandera2,bandera3,bandera4,bandera5,bandera6;
		bandera1=true;
		bandera4=true;

		uint16_t respuesta1,respuesta2,respuesta3,respuesta4;
		x1=50;
		y1=75;
		x2=50;
		y2=100;
		x3=50;
		y3=125;
		x4=50;
		y4=180;
		color=ILI9341_PINK;
		fondo=ILI9341_YELLOW;
		fuente1=font_16x26;
		fuente2=font_11x18;
		strcpy(oracion1,"Esta a punto de ");
		strcpy(oracion2,"arrancar la ");
		strcpy(oracion3,"encuesta");
		strcpy(oracion4,"SI:arriba NO:abajo");
		ptr1=& oracion1;
		ptr2=& oracion2;
		ptr3=& oracion3;
		ptr4=& oracion4;
		ptr5=&fuente1;

		uint8_t init;
		init=ILI9341Init(SPI_1, GPIO1, GPIO3,GPIO5);//inicializamos LCD
		ILI9341Rotate(ILI9341_Landscape_1);

		ILI9341Fill(fondo);
		ILI9341DrawString(x1,y1, ptr1, ptr5, color, fondo);
		ILI9341DrawString(x2,y2, ptr2, ptr5, color, fondo);
		ILI9341DrawString(x3,y3, ptr3, ptr5, color, fondo);
		ptr5=&fuente2;
		ILI9341DrawString(x4,y4, ptr4, ptr5, color, fondo);

		bandera1=true;
		//SwitchesInit();

		//SwitchActivInt(SWITCH_1,Tecla1);
		//SwitchActivInt(SWITCH_2,Tecla2);

		respuesta=NULO;

    while(1)
{
    	DelaySec(6);
    	ILI9341Fill(fondo);
    	while(bandera1==true){
    				fuente1=font_16x26;
    				ptr5=&fuente1;
    				strcpy(oracion2,"Sos estudiante?");
    				ILI9341DrawString(x2,y2, ptr2, ptr5, color, fondo);
    				respuesta=Respuesta();
    				if(respuesta==NULO)
    				{
    				}
    				else{
    					if (respuesta==SI){
    					    	bandera1=false;
    					    	bandera2=true;

    					}
    					else {
    							bandera1=false;
    							bandera2=true;
    							bandera4=true;
    					}
    				}

    	}

    	ILI9341Fill(fondo);
    	respuesta1=respuesta;
		respuesta=NULO;
    	if(respuesta1==SI){
    			while(bandera2==true){
    									fuente1=font_16x26;
    				    				ptr5=&fuente1;
    					    			strcpy(oracion2,"Te gusta Electro-");
    					    			ILI9341DrawString(x2-20,y2, ptr2, ptr5, color, fondo);
    					    			strcpy(oracion3,"nica Programable?");
    					    			ILI9341DrawString(x3-20,y3, ptr3, ptr5, color, fondo);
    					   				respuesta=Respuesta();

    					    			if(respuesta==NULO){
    					    			}
    					    			else{
    					    			if (respuesta==SI){
    					   					bandera2=false;
    					   					bandera3=true;
    					   				}
    					    			else{
    					    				bandera2=false;
    					    				bandera3=true;
    					    			}
    					    			}
    			}

    			ILI9341Fill(fondo);
    			respuesta2=respuesta;
    			respuesta=NULO;

    			while(bandera3==true){
    					    			if(respuesta2==SI){


														strcpy(oracion2,"Gracias por");
														ILI9341DrawString(x2,y2, ptr2, ptr5, color, fondo);
														strcpy(oracion3,"participar! :)");
														ILI9341DrawString(x3,y3, ptr3, ptr5, color, fondo);
														bandera4=false;
														respuesta=NULO;}



    					    			else{
    					    				strcpy(oracion2,"Gracias por");
    					    				ILI9341DrawString(x2,y2, ptr2, ptr5, color, fondo);
    					    				strcpy(oracion3,"participar! :(");
    					    				ILI9341DrawString(x3,y3, ptr3, ptr5, color, fondo);
    					    				bandera4=false;
    					    				respuesta=NULO;


    					    			}

    				}
    	}

    	else{
    		while(bandera4==true){

    		    					    strcpy(oracion2,"Sos docente o");
    		    					    ILI9341DrawString(x2,y2, ptr2, ptr5, color, fondo);
    		    					    strcpy(oracion3,"ayudante?");
    		    					    ILI9341DrawString(x3,y3, ptr3, ptr5, color, fondo);
    		    					   	respuesta=Respuesta();
    		    					    if(respuesta==NULO){}
    		    					    else{
    		    					    if (respuesta==SI){
    		    					   			bandera4=false;
    		    					   			bandera5=true;
    		    					   	}
    		    						else {
    		    								bandera4=false;
    		    								bandera5=true;
    		    					   	}
    		    					    }
    		}
    		   					    			ILI9341Fill(fondo);
    		  					    			respuesta3=respuesta;
    		  					    			respuesta=NULO;



    				while(bandera5==true){
    		    			if(respuesta3==SI){
    									strcpy(oracion2,"te gusta");
    									ILI9341DrawString(x2-20,y2, ptr2, ptr5, color, fondo);
    									strcpy(oracion3,"nuestro proyecto?");
    									ILI9341DrawString(x3-20,y3, ptr3, ptr5, color, fondo);
    		    					   	respuesta=Respuesta();
    									if(respuesta==NULO){}
    									else{
    		    					    	if (respuesta==SI){
    		    					    			bandera5=false;
    		    					    			bandera6=true;
    		    					    			respuesta4=respuesta;
    		    							}
    		    					    	else {
    		    					    			bandera5=false;
    		    					    			bandera6=true;
    		    					    			respuesta4=respuesta;
    		    					    	}

    									}
    		    			}
    		    			else{
    		    				strcpy(oracion2,"Gracias por");
    		    				ILI9341DrawString(x2,y2, ptr2, ptr5, color, fondo);
    		    				strcpy(oracion3,"participar! :/");
    		    				ILI9341DrawString(x3,y3, ptr3, ptr5, color, fondo);
    		    				//bandera5=false; fin
    		    				respuesta=NULO;
    		    				}
    				}
    				ILI9341Fill(fondo);

    				while(bandera6==true){
    		    			if(respuesta4==SI){
									strcpy(oracion2,"Gracias por");
									ILI9341DrawString(x2,y2, ptr2, ptr5, color, fondo);
									strcpy(oracion3,"participar :)");
									ILI9341DrawString(x3,y3, ptr3, ptr5, color, fondo);
									//fin
    		    			}

    		    			else{
									strcpy(oracion2,"Gracias por");
									ILI9341DrawString(x2,y2, ptr2, ptr5, color, fondo);
									strcpy(oracion3,"participar! :(");
									ILI9341DrawString(x3,y3, ptr3, ptr5, color, fondo);
									//fin
									respuesta=NULO;
							}
    				}
    	}


} //fin while



	return 0;
}

/*==================[end of file]============================================*/

