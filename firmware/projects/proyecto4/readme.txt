BOTONES_EOC

Este proyecto consiste en realizar una encuesta cuyas preguntas se muestran en una pantalla de un LCD.
A medida que avanza esta encuesta, la persona que la responda lo hace a traves de un electrooculograma (EOG),
lo cual si su respuesta es SI, la persona tendrá que mirar hacia arriba para enviar una señal positiva
y si es NO, la persona mira hacia abajo enviando una señal negativa.
Si no hace nada, el EOG de por si presenta ruido el cual definimos los limites superior e inferior con el osciloscopio;
por lo tanto este rango se define como NULO en el programa ya que nos da a entender que la persona aún no responde.

