/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
#include "math.h"
/*==================[macros and definitions]=================================*/

/*Escriba una función que reciba un dato de 32 bits,  la cantidad de dígitos de salida y un puntero a un arreglo donde se
 * almacene los n dígitos. La función deberá convertir el dato recibido a BCD, guardando cada uno de los dígitos de salida en el arreglo pasado como puntero.

BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{
}
 */
/*==================[internal functions declaration]=========================*/
void BinaryToBcd(int32_t data, uint8_t digits,uint32_t *bcd_number){
	uint8_t i;
	int32_t aux;
	int32_t DigSalida;
	for(i=0;i<digits;i++){
    	aux=pow(10,digits-1-i);
    	DigSalida=data/aux;
		bcd_number[digits-1-i]=DigSalida;
		printf ("%d \r\n",DigSalida);
		data=data%aux;

		if (i == digits-2)
			    	{

			    		DigSalida = data%aux;
			    		bcd_number [digits-2-i]= DigSalida;
			    		printf ("%d \r\n",DigSalida);
			    		i++;
			    	}

		DigSalida=data;
			    }
						}

int main(void) {
	int32_t datos;
	uint8_t nro_digitos;
	uint32_t arreglo_nros[3];

	datos=123;
	nro_digitos=3;
	BinaryToBcd(datos,nro_digitos,arreglo_nros);



    //while(1)
    //{}
	return 0;
		}

/*==================[end of file]============================================*/

