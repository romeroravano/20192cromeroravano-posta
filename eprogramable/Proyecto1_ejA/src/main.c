/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdio.h"
/*==================[macros and definitions]=================================*/
#define on 1
#define off 0
#define toggle 2
typedef struct
		{
		    uint8_t n_led;       // indica el número de led a controlar
		    uint8_t n_ciclos;   //indica la cantidad de cilcos de encendido/apagado
		    uint8_t periodo;    //indica el tiempo de cada ciclo
		    uint8_t mode;       //ON, OFF, TOGGLE
		} leds;
/*==================[internal functions declaration]=========================*/
void OperarLed (leds *x){
	uint8_t i;
	switch(x->mode)
		{
		case on:
				switch(x->n_led)
				{
					case 1: printf("Encender led 1");
							break;
					case 2: printf("Encender led 2");
							break;
					case 3: printf("Encender led 3");
							break;
					default: printf("Error 404");
				}; break;

		case off:
				switch(x->n_led)
				{
					case 1: printf("Apagar led 1");
							break;
					case 2:  printf("Apagar led 2");
							break;
					case 3:  printf("Apagar led 3");
							break;
					default: printf("Error 404");
							}; break;
		case toggle:
						for(i=0;i<x->n_ciclos;i++){

									switch(x->n_led)
									{
										case 1: printf("Parpadear led 1");
												break;
										case 2: printf("Parpadear led 2");
												break;
										case 3: printf("Parpadear led 3");
												break;
										default: printf("Error 404");
		}; break;
};
		};
}


int main(void)
{
	leds my_leds;
	my_leds.mode= off;
	my_leds.n_led=1;
	my_leds.n_ciclos=1;
	leds *punA;
	punA=&my_leds;

	OperarLed(punA);


    //while(1){}
	return 0;
}

/*==================[end of file]============================================*/

