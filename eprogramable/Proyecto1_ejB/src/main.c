/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
#include <stdio.h>
#include <string.h>
/*==================[macros and definitions]=================================*/





/*==================[internal functions declaration]=========================*/
typedef struct
{
	char txt[10];
	void (*doit)();
} menuEntry;

void LedOn()
{
	printf(" LED ON \r\n");
}
void LedOff()
{
	printf(" LED OFF \r\n");
}
void LedToggle()
{
	printf(" LED TOGGLE \r\n");
}

void EjecutarMenu (menuEntry *menu, int option)
{
	printf(menu[option].txt);
	menu[option].doit();
}


int main(void)
{
	//menuEntry menuPrincipal []={{"Etiqueta 1",LedToggle},{"Etiqueta 2",LedOff},{"Etiqueta 3", LedOn}};
		menuEntry menuPrincipal[3];
		strcpy(menuPrincipal[0].txt,"Etiqueta1");
		strcpy(menuPrincipal[1].txt,"Etiqueta2");
		strcpy(menuPrincipal[2].txt,"Etiqueta3");


		menuPrincipal [0].doit=LedToggle;
		menuPrincipal [1].doit=LedOff;
		menuPrincipal [2].doit=LedOn;


	EjecutarMenu (menuPrincipal, 2);

	return 0;
}

/*==================[end of file]============================================*/

